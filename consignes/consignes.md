# Enigme quantique N°1 : la porte du trésor

- un trésor est à trouver, gardé derrière l'une ou l'autre des deux portes;

2 gardiens : 
    - un dit la vérité, l'autre ment;
    - chaque gardien sait si l'autre ment;

Défi :

- poser une question à un seul de ces gardiens pour obtenir la localisation du trésor (porte de gauche ou porte de droite).

Solution :

- la question à poser est : quelle porte l'autre gardien me dirait de ne PAS prendre?

- explications : les réponses des deux gardiens sont toujours l'opposé l'une de l'autre si l'on demande : "derrière quelle porte est le trésor ou derrière qu'elle porte n'est pas le trésor?". 

Si l'on demande : "quelle porte l'autre gardien me dirait de ne PAS prendre?", les deux gardiens donneront la même réponse, la bonne réponse :

    - imaginons que le trésor est caché derrière la porte de droite, et que le gardien de droite ment :

        - si on demande au gardien de droite : "quelle porte l'autre gardien me dirait de ne PAS prendre?", il ment et va répondre "l'autre gardien dirait de ne PAS prendre la porte de droite".

        - si on demande au gardien de gauche : "quelle porte l'autre gardien me dirait de ne PAS prendre?", il va dire la vérité et comme il sait que le gardien de droite ment, il va répondre "l'autre gardien dirait de ne PAS prendre la porte de droite".

# Rappels :

- la porte Hadamard permet d'obtenir des probabilités de 50% pour l'acquisition d'un état ou de l'autre ("0" ou "1") et d'explorer tous les cas de figure de l'énigme en parallèle.

- les deux gardiens savent où se trouve le trésor, on leur attribue à chacun un Qbit. 

Rappel : 
    - gardien de gauche = q1, gardien de droite = q0;
    - gardien de droite = état "0", gardien de gauche = état "1";

# Utilisation des portes quantiques :

- utilisation d'une porte Hadamard sur le premier Qbit (q0), suivie d'une porte CNOT/Contrôle-NOT de q0 à q1 pour placer de manière aléatoire le trésor et s'assurer que les deux gardiens connaissent la même information;

Ainsi, la situation où le trésor se trouve derrière la porte de droite et celle où le trésor se trouve derrière la porte de gauche sont superposées : les Qbits se trouvent à la fois dans l'état "00" et "11";

NB : si on mesurait ces Qbits (cad si on leur demandait derrière qu'elle porte se trouve le trésor), les deux gardiens donneraient la même réponse : les deux Qbits sont alors intriqués.

# Ligne du "mensonge" :

- un des deux gardiens ment, il faut créer/représenter cette "ligne du mensonge" par l'introduction d'un nouveau Qbit, "q2";

Ainsi, si le "Qbit du mensonge" q2 est à l'état "1", c'est le gardien de gauche qui ment. On applique alors une porte X/NOT au gardien de gauche (Qbit "q1") afin d'inverser sa réponse.

NB : lorsque le gardien de droite ment, le Qbit "q2" représentant la "ligne du mensonge" est à l'état "0";

Ainsi, pour modifier (inverser) la réponse du gardien de droite, il faut préalablement appliquer une porte X/NOT au Qbit "q2" afin de le faire passer de l'état "0" à l'état "1".

Cela permettra ensuite d'appliquer une porte C-NOT/Contrôle NOT au Qbit "q0" représentant le gardien de droite et ainsi de modifier/inverser sa réponse.

Enfin, pour récupérer l'état "d'origine" du Qbit "q2", on applique de nouveau une porte X/NOT afin de faire repasser l'état du Qbit de "1" à "0";

Si l'on ajoute à cela une porte Hadamard, les deux possibilités se retrouvent en superposition d'état : la probabilité d'obtenir "0" ou "1" est de 50% pour chaque réponse.

Ainsi, à ce stade, si on interrogeait un des gardiens, on poserait la question : "derrière quelle porte se trouve le trésor?", et les deux gardiens auraient des réponses opposées.

# Les possibilités décrivant le système :

Rappel de la question : derrière quelle porte se trouve le trésor?

Rappel des réponses : porte de droite = "0"; porte de gauche = "1";

- 1ère possibilité :
    - trésor à droite et gardien de droite ment : 
        - "100" : "1" car le gardien de droite qui garde le trésor (représenté par le Qbit "q0") ment et répondra ainsi que le trésor se trouve derrière la porte de gauche, "0" car le gardien de gauche (représenté par le Qbit "q1") dit la vérité et répondra ainsi que le trésor se trouve derrière la porte de droite, "0" car comme c'est le gardien de droite qui ment, le Qbit "q2" représentant la ligne du mensonge se trouve à l'état "0";

- 2nd possibilité :
    - trésor à droite et gardien de gauche ment : 
        - "011" : "0" car le gardien de droite qui garde le trésor (représenté par le Qbit "q0") dit la vérité et répondra ainsi que le trésor se trouve derrière la porte de droite, "1" car le gardien de gauche (représenté par le Qbit "q1") ment et répondra ainsi que le trésor se trouve derrière la porte de gauche, "1" car comme c'est le gardien de gauche qui ment, le Qbit "q2" représentant la ligne du mensonge se trouve à l'état "1";

- 3ème possibilité :
    - trésor à gauche et gardien de droite ment : 
        - "010" : "0" car le gardien de droite (représenté par le Qbit "q0") ment et répondra ainsi que le trésor se trouve derrière la porte de droite, "1" car le gardien de gauche qui garde le trésor (représenté par le Qbit "q1") dit la vérité et répondra ainsi que le trésor se trouve derrière la porte de gauche, "0" car comme c'est le gardien de droite qui ment, le Qbit "q2" représentant la ligne du mensonge se trouve à l'état "0";

- 4ème possibilité :
    - trésor à gauche et gardien de gauche ment : 
        - "101" : "1" car le gardien de droite (représenté par le Qbit "q0") dit la vérité et répondra ainsi que le trésor se trouve derrière la porte de gauche, "0" car le gardien de gauche qui garde le trésor (représenté par le Qbit "q1") ment et répondra ainsi que le trésor se trouve derrière la porte de droite , "1" car comme c'est le gardien de gauche qui ment, le Qbit "q2" représentant la ligne du mensonge se trouve à l'état "1";

# Conclusion sur les possibilités décrivant le système :

- les quatre possibilités coexistent en parallèle tant que la mesure finale n'est pas relevée/prise.

# Résolution de l'énigme :

## Finalisation de la problématique et introduction des dernières portes quantiques :

- on souhaite poser la question : "quelle porte l'autre gardien me dirait de ne pas prendre?".

Pour représenter la question : "Que me dirait l'autre gardien si", on va appliquer une porte Swap à partir du gardien de droite (Qbit "q0") vers le gardien de gauche (Qbit "q1");

Comme nous demandons de ne PAS prendre, on rajoute une porte X/NOT sur chacun des gardiens (Qbits "q0" et "q1");

Ensuite, comme on sait qu'un des gardiens ment concernant la réponse de l'autre gardien : on doit donc réappliquer le "circuit du mensonge" avec la même série de portes que précédemment, à savoir :

- une porte C-NOT/Contrôle NOT partant de la "ligne du mensonge" (Qbit "q2") au gardien de gauche (Qbit "q1");

- une porte X/NOT sur la "ligne du mensonge" (Qbit "q2");

- une porte C-NOT/Contrôle NOT partant de la "ligne du mensonge" (Qbit "q2") au gardien de droite (Qbit "q0");

- une seconde porte X/NOT sur la "ligne du mensonge" (Qbit "q2");

## Rappels sur la problématique/cas d'utilisation et l'état de "départ/fin" du système :

Problématique/cas d'utilisation :

- trésor derrière la porte de droite, avec le gardien de droite qui ment;

- question posée : quelle porte l'autre gardien me dirait de ne PAS prendre?

Détail états du système :

3 digits : 
    1er bit => Qbit "q0", gardien de droite;
    2eme bit => Qbit "q1", gardien de gauche;
    3eme bit => Qbit "q2", ligne du mensonge;  

Explications sur l'état de "départ/fin" du système : 

La question posée étant "quelle porte l'autre gardien me dirait de ne pas prendre", les valeurs des trois bits (ceux des deux gardiens et celui de la "ligne de mensonge") vont être conditionnées par les réponses données par chaque gardien ainsi que par le "mensonge" de l'un ou de l'autre gardien.

Rappel : 
    - réponse => porte de droite => "0"; 
    - réponse => porte de gauche => "1"; 

Ainsi, en tenant compte de la question posée à chaque gardien, de l'emplacement du trésor et de la "sincérité" ou non de chaque gardien, on obtient :
    - "0" : le gardien de droite ment et réponds donc que le gardien de gauche dirait de ne pas prendre la porte de droite;
    - "0" : le gardien de gauche sait que le gardien de droite ment et réponds donc que le gardien de droite dirait de ne pas prendre la porte de droite;
    - "0" : comme le gardien de droite ment, le Qbit "q2" représentant la "ligne du mensonge" est à l'état "0";

## Test du circuit complet avec "blocs", portes et états :

- Etat de "départ" du système :
    - "000";

- Bloc 1 :
    - application d'une porte C-NOT/Contrôle NOT partant de la "ligne du mensonge" (Qbit "q2") au gardien de gauche (Qbit "q1").

    - état du système : "000";

    - application d'une porte X/NOT sur la "ligne du mensonge" (Qbit "q2");

    - état du système : "001";

    - application d'une porte C-NOT/Contrôle NOT partant de la "ligne du mensonge" (Qbit "q2") au gardien de droite (Qbit "q0").

    - état du système : "101";

    - application d'une porte X/NOT sur la "ligne du mensonge" (Qbit "q2");

    - état du système : "100";

- Bloc 2 :
    - application d'une porte Swap entre le gardien de droite (Qbit "q0") et le gardien de gauche (Qbit "q1");

    - état du système : "010";

    - application d'une porte X/NOT sur le gardien de droite (Qbit "q0");

    - application d'une porte X/NOT sur le gardien de gauche (Qbit "q1");

    - état du système : "100";

    - application d'une porte C-NOT/Contrôle NOT partant de la "ligne du mensonge" (Qbit "q2") au gardien de gauche (Qbit "q1").

    - état du système : "100";

    - application d'une porte X/NOT sur la "ligne du mensonge" (Qbit "q2");

    - état du système : "101";

    - application d'une porte C-NOT/Contrôle NOT partant de la "ligne du mensonge" (Qbit "q2") au gardien de droite (Qbit "q0").

    - état du système : "001";

    - application d'une porte X/NOT sur la "ligne du mensonge" (Qbit "q2");

    - état du système : "000";

## Conclusion :

- Il ne reste finalement plus qu'à poser la question "quelle porte l'autre gardien me dirait de ne pas prendre?" à l'un des deux gardiens, c'est à dire mesurer l'un des deux premiers Qbits ("q0" ou "q1");

- On peut mesurer le Qbit de l'autre gardien pour vérifier l'obtention de la même réponse afin de s'assurer que les deux gardiens donnent bien cette même réponse.

Au stade de la "fin" du système, on ignore toujours lequel des deux gardiens ment : le Qbit de la "ligne du mensonge" (Qbit "q2"), se trouve toujours en superposition d'états.

Cependant, on peut mesurer "q2" afin de déterminer lequel des deux gardiens mentait.

On vient ici d'illustrer la première possibilité du système en partant du postulat/cas d'utilisation suivant : le trésor est à droite et le gardien de droite ment.

## Différences ordinateurs classiques/quantiques :

- Contrairement aux ordinateur classiques, les quatre possibilités coexistent en parallèle dans l'ordinateur quantique jusqu'à ce que la mesure finale soit prise/relevée sur l'un des deux premiers Qbits.

La série de portes incluses dans le système fonctionne pour toutes les possibilités.

## Probabilités de mesure :

Rappel : rappel des quatre possibilités :
    - 1 : trésor à droite et gardien de droite ment;
    - 2 : trésor à droite et gardien de gauche ment;
    - 3 : trésor à gauche et gardien de droite ment;
    - 4 : trésor à gauche et gardien de gauche ment;

- En prenant la question "quelle porte l'autre gardien me dirait de ne pas prendre?", si le calcul est exécuté une seule fois, chacune des quatre possibilités a 25% de chances de survenir.

2ème Rappel : pour les résultats, les Qbits sont notés de la façon suivante : le Qbit "q0" est à droite, le Qbit "q1" est au milieu et le Qbit "q2" est à gauche.

Illustration/rappel des résultats des possibilités : 

Rappel : 
    - réponse => porte de droite => "0"; 
    - réponse => porte de gauche => "1"; 

"000" : le trésor est à droite et le gardien de droite ment :

- Notation de gauche à droite :

    - "0" : comme le gardien de droite ment, le Qbit "q2" représentant la "ligne du mensonge" est à l'état "0";

    - "0" : le gardien de gauche sait que le gardien de droite ment et réponds donc que le gardien de droite dirait de ne pas prendre la porte de droite;

    - "0" : le gardien de droite ment et réponds donc que le gardien de gauche dirait de ne pas prendre la porte de droite;

"011" : le trésor est à gauche et le gardien de droite ment :

    - "0" : comme le gardien de droite ment, le Qbit "q2" représentant la "ligne du mensonge" est à l'état "0";

    - "1" : le gardien de gauche sait que le gardien de droite ment et réponds donc que le gardien de droite dirait de ne pas prendre la porte de gauche;

    - "1" : le gardien de droite ment et réponds donc que le gardien de gauche dirait de ne pas prendre la porte de gauche;
    
"100" : le trésor est à droite et le gardien de gauche ment :

    - "1" : comme le gardien de gauche ment, le Qbit "q2" représentant la "ligne du mensonge" est à l'état "1";

    - "0" : le gardien de gauche ment et réponds donc que le gardien de droite dirait de ne pas prendre la porte de droite;

    - "0" : le gardien de droite sait que le gardien de gauche ment et réponds donc que le gardien de gauche dirait de ne pas prendre la porte de droite;

"111" : le trésor est à gauche et le gardien de gauche ment :

    - "1" : comme le gardien de gauche ment, le Qbit "q2" représentant la "ligne du mensonge" est à l'état "1";

    - "1" : le gardien de gauche ment et réponds donc que le gardien de droite dirait de ne pas prendre la porte de gauche;

    - "1" : le gardien de droite sait que le gardien de gauche ment et réponds donc que le gardien de gauche dirait de ne pas prendre la porte de gauche;












































